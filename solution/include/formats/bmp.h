//
// Created by golan on 27.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include "../image/image.h"
#include <stdio.h>

enum read_status {
    READ_OK = 0,
    READ_NULL_STREAM = 4001,
    READ_INVALID_IMAGE = 4002,
    READ_INVALID_SIGNATURE = 4003,
    READ_INVALID_BITS = 4004,
    READ_INVALID_AMOUNT = 4005,
    READ_INVALID_HEADER = 4006,
    READ_ERROR = 4007
};


enum write_status {
    WRITE_OK = 0,
    WRITE_NULL_STREAM = 5001,
    WRITE_INVALID_BITS = 5002,
    WRITE_INVALID_IMAGE = 5003,
    WRITE_ERROR = 5004
};

enum read_status from_bmp(FILE *in, struct maybe_image *src);

enum write_status to_bmp(FILE *out, struct maybe_image *src);

void print_read_status(enum read_status status);
void print_write_status(enum write_status status);
//void print_read_status(enum read_status status);
//void print_write_status(enum write_status status);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
