//
// Created by golan on 27.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H

#include "../image/image.h"

struct maybe_image rotate(struct image src);

#endif
