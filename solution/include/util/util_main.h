//
// Created by golan on 18.01.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_UTIL_MAIN_H
#define ASSIGNMENT_IMAGE_ROTATION_UTIL_MAIN_H
#include "../formats/bmp.h"
#include "../image/image.h"
#include "util.h"

#include <stdio.h>

enum read_status read_bmp_from_file(const char *file_name_in, struct maybe_image *source);
enum write_status write_bmp_to_file(const char *file_name_out, struct maybe_image *source);

#endif //ASSIGNMENT_IMAGE_ROTATION_UTIL_MAIN_H
