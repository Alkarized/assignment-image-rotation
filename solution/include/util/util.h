//
// Created by golan on 27.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_UTIL_H
#define ASSIGNMENT_IMAGE_ROTATION_UTIL_H

#include <stdbool.h>
#include <stdio.h>

enum file_open_status{
    FILE_OPEN_OK = 0,
    FILE_OPEN_NULL_ARGUMENT_FILE = 1001,
    FILE_OPEN_NULL_ARGUMENT_FILE_NAME = 1002,
    FILE_OPEN_NULL_ARGUMENT_MODE = 1003,
    FILE_OPEN_ERROR = 1004
};

enum file_close_status{
    FILE_CLOSE_OK = 0,
    FILE_CLOSE_NULL_ARGUMENT = 2001,
    FILE_CLOSE_ERROR = 2002
};

enum file_open_status open_file(FILE **in, const char *file_name, const char *mode);
enum file_close_status close_file(FILE *out);
void print_file_open_status(enum file_open_status status);
void print_file_close_status(enum file_close_status status);
#endif //ASSIGNMENT_IMAGE_ROTATION_UTIL_H
