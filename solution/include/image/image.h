//
// Created by golan on 27.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <malloc.h>
#include <stdbool.h>
#include <stdint.h>

struct __attribute__ ((packed)) pixel{
    uint8_t b, g, r;
};

struct image{
    uint64_t width, height;
    struct pixel* data;
};

struct maybe_image{
    bool valid;
    struct image img;
};

static const struct maybe_image none_image = {0};

static inline struct maybe_image some_image(struct image img){
    return (struct maybe_image) {.valid = true, .img = img };
}

struct maybe_image create_empty_image(const uint64_t width, const uint64_t height);
void destroy_image(const struct maybe_image *src);

static inline size_t get_index(uint64_t width, size_t i, size_t j){
    return i * width + j;
}
#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
