//
// Created by golan on 18.01.2022.
//

#include "../../include/util/util_main.h"

enum read_status read_bmp_from_file(const char *file_name_in, struct maybe_image *source){
    FILE *in = NULL;
    enum file_open_status in_open_status = open_file(&in, file_name_in, "rb");

    if (in_open_status){
        print_file_open_status(in_open_status);
        return READ_ERROR;
    }

    enum read_status bmp_read_status = from_bmp(in, source);

    print_file_close_status(close_file(in));

    return bmp_read_status;

}
enum write_status write_bmp_to_file(const char *file_name_out, struct maybe_image *source){
    FILE *out = NULL;
    enum file_open_status out_open_status = open_file(&out, file_name_out, "wb");

    if (out_open_status){
        print_file_open_status(out_open_status);
        return WRITE_ERROR;
    }

    enum write_status bmp_write_status = to_bmp(out, source);

    print_file_close_status(close_file(out));

    return bmp_write_status;
}
