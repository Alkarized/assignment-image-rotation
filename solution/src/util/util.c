//
// Created by golan on 27.12.2021.
//

#include "../../include/util/util.h"

enum file_open_status open_file(FILE **in, const char *file_name, const char *mode){
    if (!in) return FILE_OPEN_NULL_ARGUMENT_FILE;
    if (!file_name) return FILE_OPEN_NULL_ARGUMENT_FILE_NAME;
    if (!mode) return FILE_OPEN_NULL_ARGUMENT_MODE;
    *in = fopen(file_name, mode);
    if (!*in) return FILE_OPEN_ERROR;
    return *in == NULL;

}

enum file_close_status close_file(FILE *out){
    if (!out) return FILE_CLOSE_NULL_ARGUMENT;
    if (fclose(out)) return FILE_CLOSE_ERROR;
    return FILE_CLOSE_OK;
}

void print_file_open_status(enum file_open_status status){
    const char array_of_statuses[5][50] = {{"File's opened\n"}, {"Got null file\n"}, {"Got file null name\n"}, {"Got null mode\n"},
                                           {"Error happened, can't open file\n"}};
    if (status == 0){
        fprintf(stderr, "%s", array_of_statuses[status]);
    } else {
        fprintf(stderr,  "%s", array_of_statuses[status + 1000]);
    }

}

void print_file_close_status(enum file_close_status status){
    const char array_of_statuses[3][50] = {{"File's closed\n"}, {"Got file null argument\n"},
                                           {"Error happened, can't close file\n"}};
    if (status == 0){
        fprintf(stderr, "%s", array_of_statuses[status]);
    } else {
        fprintf(stderr, "%s",array_of_statuses[status + 2000]);
    }
}

