#include "../include/util/util_main.h"
#include "../include/transformations/rotate.h"

#include <stdio.h>

int main( int argc, char** argv ) {
    if (argc != 3) {
        fprintf(stderr, "Invalid amount of args\n");
        return 1;
    }
    const char *file_name_in = argv[1];
    const char *file_name_out = argv[2];

    struct maybe_image source = {0};

    enum read_status bmp_read_status = read_bmp_from_file(file_name_in, &source);

    if (bmp_read_status){
        destroy_image(&source);
        print_read_status(bmp_read_status);
        return bmp_read_status;
    }

    struct maybe_image rotated_image = rotate(source.img);

    destroy_image(&source);

    enum write_status  bmp_write_status = write_bmp_to_file(file_name_out, &rotated_image);

    if (bmp_write_status){
        destroy_image(&rotated_image);
        print_write_status(bmp_write_status);
        return bmp_write_status;
    }

    destroy_image(&rotated_image);

    return 0;
}

