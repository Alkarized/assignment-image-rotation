//
// Created by golan on 27.12.2021.
//


#include "../../include/image/image.h"


struct maybe_image create_empty_image(const uint64_t width, const uint64_t height) {
    if (!(width && height)) return none_image;

    struct image img = {0};

    img.width = width;
    img.height = height;

    img.data = malloc(sizeof(struct pixel) * width * height);
    if (img.data) {
        return some_image(img);
    } else {
        return none_image;
    }

}

void destroy_image(const struct maybe_image *src) {
    free(src->img.data);

}

