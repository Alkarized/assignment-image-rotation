//
// Created by golan on 27.12.2021.
//

#include "../../include/transformations/rotate.h"

struct maybe_image rotate(struct image src) {
    struct maybe_image dest = create_empty_image(src.height, src.width);
    if (dest.valid) {
        for (size_t y = 0; y < src.height; y++) {
            for (size_t x = 0; x < src.width; x++) {
                dest.img.data[get_index(src.height, (x + 1), - ( 1 + y))] = src.data[get_index(src.width, y, x)]; // Поворот налево
                //dest.img.data[src.height  * (j + 1) - (1 + i)] = src.data[(src.height - i) * src.width + j]; // Поворот направо
                //dest.img.data[i * dest.img.width + j] = src.data[(src.height - j - 1)*src.width + i];
            }
        }

    }

    return dest;

}
