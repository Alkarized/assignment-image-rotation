//
// Created by golan on 27.12.2021.
//

#include "../../include/formats/bmp.h"


#include <inttypes.h>
#include <stdint.h>

#pragma pack(push, 1)
typedef struct {
    uint16_t bf_type;
    uint32_t b_file_size;
    uint32_t bf_reserved;
    uint32_t b_off_bits;
    uint32_t bi_size;
    uint32_t bi_width;
    uint32_t bi_height;
    uint16_t bi_planes;
    uint16_t bi_bit_count;
    uint32_t bi_compression;
    uint32_t bi_size_image;
    uint32_t bi_x_pels_per_meter;
    uint32_t bi_y_pels_per_meter;
    uint32_t bi_clr_used;
    uint32_t bi_clr_important;

} bmp_header;
#pragma pack(pop)

#define FOUR_BYTES 4
#define ONE_PIXEL 3

#define BF_TYPE 19778
#define BIT_COUNT 24
#define BI_PLANES 1
#define ZERO 0
#define BI_SIZE 40

static enum read_status read_bmp_header(FILE *in, bmp_header *header);

static enum read_status read_bmp_pixels(FILE *in, struct image img);

static bmp_header create_header(struct image const img);

static enum write_status write_bmp_pixels(FILE *out, struct image const img);

static inline uint8_t
get_padding(uint64_t
width){
return (uint8_t)(FOUR_BYTES - (width*ONE_PIXEL) % FOUR_BYTES) % FOUR_BYTES;
}

enum read_status from_bmp(FILE *in, struct maybe_image *src) {
    if (!in) return READ_NULL_STREAM;

    bmp_header header = {0};
    enum read_status header_status = read_bmp_header(in, &header);
    if (header_status) return header_status;

    *src = create_empty_image(header.bi_width, header.bi_height);

    if (!src->valid) return READ_INVALID_IMAGE;

    enum read_status pixels_status = read_bmp_pixels(in, src->img);
    if (pixels_status) return pixels_status;
    //print_header(&header);
    return READ_OK; //Можно сделать также как и в to_bmp, через лоигческое или, так где-то в жавке было, присвоить значение статусам и тд.
}

static enum read_status read_bmp_header(FILE *in, bmp_header *header) {
    if (fseek(in, 0, SEEK_END)) return READ_INVALID_SIGNATURE;
    if (ftell(in) < sizeof(bmp_header)) return READ_INVALID_HEADER;
    rewind(in);
    if (!fread(header, sizeof(bmp_header), 1, in)) return READ_ERROR;
    return READ_OK;
}

static enum read_status read_bmp_pixels(FILE *in, struct image img) {
    const uint64_t padding = get_padding(img.width);
    //printf("%"PRId64, padding);
    for (size_t i = 0; i < img.height; ++i) {
        if (fread(img.data + i * img.width, sizeof(struct pixel), img.width, in) != img.width) return READ_INVALID_BITS;
        if (fseek(in, padding, SEEK_CUR)) return READ_INVALID_AMOUNT;
        /*for (int j = 0; j < img.width; j++){
            //printf("b - " "%"PRId8",g - %"PRId8 ", r - %"PRId8 "\t\t", img.data[j + i*img.width].b,  img.data[j + i*img.width].g,  img.data[j + i*img.width].r);
        }*/
        //printf("\n");/*

    }

    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct maybe_image *src) {
    if (!out) return WRITE_NULL_STREAM;
    if (!src->valid) return WRITE_INVALID_IMAGE;

    bmp_header header = create_header(src->img);
    //print_header(&header);
    if (!fwrite(&header, sizeof(bmp_header), 1, out)) return WRITE_ERROR;
    return write_bmp_pixels(out, src->img);
}

static bmp_header create_header(struct image img) {
    uint32_t temp_size_image = (img.width * sizeof(struct pixel) + get_padding(img.width)) * img.height;
    return (bmp_header) {
            .bf_type = BF_TYPE,
            .bi_size_image = temp_size_image,
            .b_file_size = temp_size_image + sizeof(bmp_header),
            .bf_reserved = ZERO,
            .b_off_bits = sizeof(bmp_header),
            .bi_size = BI_SIZE,
            .bi_width = img.width,
            .bi_height = img.height,
            .bi_planes = BI_PLANES,
            .bi_bit_count = BIT_COUNT,
            .bi_compression = ZERO,
            .bi_x_pels_per_meter = ZERO,
            .bi_y_pels_per_meter = ZERO,
            .bi_clr_used = ZERO,
            .bi_clr_important = ZERO
    };
}


static enum write_status write_bmp_pixels(FILE *out, struct image const img) {
    const uint8_t padding = get_padding(img.width);
    //printf("%"PRId8, padding);
    const uint8_t zeroes[3] = {0}; //3 enough, coz we have these variants {0, 1, 2, 3} of padding;
    if (img.data != NULL) {
        for (size_t i = 0; i < img.height; ++i) {
            if (!fwrite(img.data + i * img.width, sizeof(struct pixel), img.width, out) ||
                !fwrite(zeroes, padding, 1, out))
                return WRITE_ERROR;
        }
    }
    return WRITE_OK;

}

void print_read_status(enum read_status status) {
    const char array_of_statuses[8][50] = {{"Read done\n"},
                                     {"Got null stream\n"},
                                     {"Detected invalid image\n"},
                                     {"Read invalid signature \n"},
                                     {"Read invalid amount of bits\n"},
                                     {"Stuck in reading\n"},
                                     {"Got invalid header\n"},
                                     {"Got error while reading file\n"}};
    if (status == 0) {
        fprintf(stderr, "%s", array_of_statuses[status]);
    } else {
        fprintf(stderr, "%s", array_of_statuses[status + 4000]);
    }

}

void print_write_status(enum write_status status) {
    const char array_of_statuses[5][50] = {{"Write done\n"},
                                     {"Got null stream\n"},
                                     {"Got error while writing image\n"},
                                     {"Incorrect image detected\n"},
                                     {"Got error while parsing file\n"}};
    if (status == 0) {
        fprintf(stderr, "%s", array_of_statuses[status]);
    } else {
        fprintf(stderr, "%s", array_of_statuses[status + 5000]);
    }
}
/*
static void print_header(bmp_header* header) {
    printf("bf_type: %u\n", header->bf_type);
    printf("b_file_size: %u\n", header->b_file_size);
    printf("bf_reserved: %u\n", header->bf_reserved);
    printf("b_off_bits: %u\n", header->b_off_bits);
    printf("bi_size: %u\n", header->bi_size);
    printf("bi_width: %u\n", header->bi_width);
    printf("bi_height: %u\n", header->bi_height);
    printf("bi_planes: %u\n", header->bi_planes);
    printf("bi_bit_count: %u\n", header->bi_bit_count);
    printf("bi_compression: %u\n", header->bi_compression);
    printf("bi_size_image: %u\n", header->bi_size_image);
    printf("bi_x_pels_per_meter: %u\n", header->bi_x_pels_per_meter);
    printf("bi_y_pels_per_meter: %u\n", header->bi_y_pels_per_meter);
    printf("bi_clr_used: %u\n", header->bi_clr_used);
    printf("bi_clr_used: %u\n", header->bi_clr_used);
    printf("\n");
}
*/
